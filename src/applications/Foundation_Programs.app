<?xml version="1.0" encoding="UTF-8"?>
<CustomApplication xmlns="http://soap.sforce.com/2006/04/metadata">
    <defaultLandingTab>standard-home</defaultLandingTab>
    <label>Foundation Programs</label>
    <tab>standard-Account</tab>
    <tab>standard-Contact</tab>
    <tab>Referral__c</tab>
    <tab>standard-Campaign</tab>
    <tab>Survey__c</tab>
    <tab>Service_Hours_Record__c</tab>
    <tab>standard-report</tab>
    <tab>standard-Dashboard</tab>
</CustomApplication>
